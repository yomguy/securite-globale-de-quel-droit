
function videoEnded(video) {
  video.load();
};

new Vue({
  el: '#app',
  vuetify: new Vuetify(),
  data () {
    return {
      features: [
        {
          img: 'https://files.parisson.com/LSG/img/OlivierCahn00001.tif.jpg',
          title: 'Olivier Cahn',
          text: 'Professeur à l’Université de Tours',
        },
        {
          img: 'https://files.parisson.com/LSG/img/ChristineLazergue00001.tif.jpg',
          title: 'Christine Lazerges',
          text: 'Professeure émérite de l’Université Paris 1- Panthéon-Sorbonne',
        },
        {
          img: 'https://files.parisson.com/LSG/img/RaphaelleParisot00011.tif.jpg',
          title: 'Raphaële Parizot',
          text: 'Professeure à l’Université Paris Nanterre',
        },
        {
          img: 'https://files.parisson.com/LSG/img/PascalBeauvais00018.tif.jpg',
          title: 'Pascal Beauvais',
          text: 'Professeur à l’Université Paris 1- Panthéon-Sorbonne',
        },

        {
          img: 'https://files.parisson.com/LSG/img/LUCIE00000.tif.jpg',
          title: 'Lucie Cluzel',
          text: 'Professeure à l’Université Paris Nanterre',
        },
        {
          img: 'https://files.parisson.com/LSG/img/Ludivine00001.tif.jpg',
          title: 'Ludivine Richefeu',
          text: 'Maîtresse de conférences à CY Cergy Paris Université',
        },
        {
          img: 'https://files.parisson.com/LSG/img/NoeWagener00011.tif.jpg',
          title: 'Noé Wagener',
          text: 'Professeur à l’Université Paris Est Créteil',
        },

      ],
      extracts: [
        {
          img: 'https://files.parisson.com/LSG/img/OlivierCahn00001.tif.jpg',
          title: 'Olivier Cahn',
          text: 'Professeur à l’Université de Tours',
          type: 'text',
          citation: '« Cette notion de sécurité globale a été pensée par les milieux néo-conservateurs américains proches des cercles militaires au moment de la fin de la guerre froide, elle constitue une rupture avec les principes hérités de la Modernité. L’ère des guerres classique Etat contre Etat semblait révolue et il fallait imaginer un nouveau paradigme sécuritaire pour justifier les budgets de l’armée et de la sécurité, lequel n’a jamais été formellement approuvé par le Parlement. Depuis 2013, la France adhère progressivement, à son tour, à cette conception purement utilitariste de la sécurité : l’État peut, au mépris des distinctions civil/militaire, guerre/paix et droit pénal/droit administratif, utiliser tous les moyens à sa disposition – armée, polices, agents privés – pour assurer sa sécurité, aussi bien à l’extérieur, qu’à l’intérieur de ses frontières ».'
        },
        {
          src: 'https://files.parisson.com/LSG/video/lsg-extrait-olivier-01.mp4',
          img: 'https://files.parisson.com/LSG/img/OlivierCahn00001.tif.jpg',
          title: 'Olivier Cahn',
          text: 'Professeur à l’Université de Tours',
          type: 'video'
        },

        {
          src: 'https://files.parisson.com/LSG/video/lsg-extrait-christine-01.mp4',
          img: 'https://files.parisson.com/LSG/img/ChristineLazergue00001.tif.jpg',
          title: 'Christine Lazerges',
          text: 'Professeure émérite de l’Université Paris 1- Panthéon-Sorbonne',
          type: 'video'
        },
        {
          img: 'https://files.parisson.com/LSG/img/ChristineLazergue00001.tif.jpg',
          title: 'Christine Lazerges',
          text: 'Professeure émérite de l’Université Paris 1- Panthéon-Sorbonne',
          type: 'text',
          citation: '« Il y a dans le titre de cette proposition de loi quelque chose d’effrayant, de l’ordre de l’énorme mensonge, la sécurité globale, totale ne peut être qu’un mirage sauf peut-être dans un monde de la transhumanité que bien peu souhaitent »'

        },
        {
          img: 'https://files.parisson.com/LSG/img/RaphaelleParisot00011.tif.jpg',
          title: 'Raphaële Parizot',
          text: 'Professeure à l’Université Paris Nanterre',
          type: 'text',
          citation: '« Avec l’article 24 de cette loi, comme avec le délit d’intrusion dans les universités, le législateur cherche à punir un acte totalement licite en s’appuyant sur l’intention de son auteur. Or, on ne peut pas connaître l’intention d’une personne, raison pour laquelle le droit pénal ne doit pas punir les intentions. En réalité, avec ce genre de délit, le législateur crée une base légale pour une intervention policière ; la loi permet aux policiers d’interpeller des personnes, de les placer en garde à vue, en sachant qu’en réalité, ils ne pourront pas les poursuivre sur le fondement de ce texte ».'
        },
        {
          src: 'https://files.parisson.com/LSG/video/lsg-extrait-raph-01.mp4',
          img: 'https://files.parisson.com/LSG/img/RaphaelleParisot00011.tif.jpg',
          title: 'Raphaële Parizot',
          text: 'Professeure à l’Université Paris Nanterre',
          type: 'video'
        },
        {
          src: 'https://files.parisson.com/LSG/video/lsg-extrait-pascal-01.mp4',
          img: 'https://files.parisson.com/LSG/img/PascalBeauvais00018.tif.jpg',
          title: 'Pascal Beauvais',
          text: 'Professeur à l’Université Paris 1- Panthéon-Sorbonne',
          type: 'video'
        },
        {
          img: 'https://files.parisson.com/LSG/img/PascalBeauvais00018.tif.jpg',
          title: 'Pascal Beauvais',
          text: 'Professeur à l’Université Paris 1- Panthéon-Sorbonne',
          type: 'text',
          citation: '« On a le sentiment d’un mouvement sans fin, d’une succession continue de lois qui donnent toujours plus de pouvoirs à l’État pour contrôler, surveiller et réprimer la population dans le cadre d’une politique de tolérance « zéro ». Mais la délinquance ne va jamais disparaître, elle est un phénomène normal, comme disait Durkheim, elle se réorganisera toujours pour s’adapter aux nouveaux moyens de surveillance. Or, le résultat ultime de ce mouvement vers toujours plus de surveillance ce sera une société )totalitaire où les citoyens seront surveillés du matin jusqu’au soir. Là, il y aura peut-être des résultats en terme de lutte contre la criminalité mais ce sera au prix d’une société Orwellienne. »'
        },

        {
          img: 'https://files.parisson.com/LSG/img/LUCIE00000.tif.jpg',
          title: 'Lucie Cluzel',
          text: 'Professeure à l’Université Paris Nanterre',
          type: 'text',
          citation: '«  Cette texte va légaliser plusieurs mesures de technopolice, plusieurs pratiques de surveillance dites intelligentes, utilisant des capteurs sonores, thermiques, des capteurs d’odeurs et aussi des capteurs d’images. Ces dispositifs sont très attentatoires aux libertés, ils menacent nos vies privées mais aussi les libertés d’opinion et d’expression parce que, logiquement, quand on se sent surveillés, on a tendance à se censurer. Or, la loi surprend par la faiblesse des garanties qui entourent l’utilisation de ces dispositifs de surveillance ».'
        },
        {
          src: 'https://files.parisson.com/LSG/video/lsg-extrait-lucie-01.mp4',
          img: 'https://files.parisson.com/LSG/img/LUCIE00000.tif.jpg',
          title: 'Lucie Cluzel',
          text: 'Professeure à l’Université Paris Nanterre',
          type: 'video'
        },
        {
          src: 'https://files.parisson.com/LSG/video/lsg-extrait-ludivine-01.mp4',
          img: 'https://files.parisson.com/LSG/img/Ludivine00001.tif.jpg',
          title: 'Ludivine Richefeu',
          text: 'Maîtresse de conférences à CY Cergy Paris Université',
          type: 'video'
        },

        {
          img: 'https://files.parisson.com/LSG/img/Ludivine00001.tif.jpg',
          title: 'Ludivine Richefeu',
          text: 'Maîtresse de conférences à CY Cergy Paris Université',
          type: 'text',
          citation: '« Le droit, pour les policiers, de porter une arme hors service leur a été accordé pendant l’état d’urgence terroriste de 2015. Comme beaucoup d’autres mesures, celle-ci a été pérennisée, intégrée au droit commun. Aujourd’hui,  avec la loi sur la sécurité globale, le législateur souhaite élargir ce droit en autorisant les policiers à porter leur arme hors service, y compris dans les lieux ouverts au public. Mais c’est le gouvernement qui, dans un second temps, aura la compétence de déterminer, par arrêté du ministre de l’Intérieur, les conditions de ce port d’arme ».'
        },
        {
          img: 'https://files.parisson.com/LSG/img/NoeWagener00011.tif.jpg',
          title: 'Noé Wagener',
          text: 'Professeur à l’Université Paris Est Créteil',
          type: 'text',
          citation: '« A mon sens, il ne faut pas saisir le débat autour de cette loi sur la sécurité globale uniquement en terme de grandes libertés fondamentales. Ce qui est en jeu à travers chacune des mesures de cette loi – qu’il s’agisse des polices municipales, des agents de sécurité privé ou de la surveillance par drone – c’est la qualité d’espace public ouvert. Ce texte, en quadrillant l’espace public, cherche à remettre en cause sa qualité de chose commune dont nous pouvons jouir et user collectivement. Il cherche à isoler et étiqueter les personnes qui circulent dans cet espace dont en principe personne ne peut être exclu ».'
        },
        {
          src: 'https://files.parisson.com/LSG/video/lsg-extrait-noe-01.mp4',
          img: 'https://files.parisson.com/LSG/img/NoeWagener00011.tif.jpg',
          title: 'Noé Wagener',
          text: 'Professeur à l’Université Paris Est Créteil',
          type: 'video'
        },


      ],
      stats: [
        ['SORTIE LE 1er FEVRIER 2021', 'ICI'],
      ],
      articles: [
        {
          src: 'https://reporterre.net/local/cache-vignettes/L720xH481/arton21770-773dc.jpg?1610790617',
          title: '« L’État nous pousse à agir comme la police »',
          text: 'par Vanessa Codaccioni',
          url: 'https://reporterre.net/Vanessa-Codaccioni-L-Etat-nous-pousse-a-agir-comme-la-police'
        },
        {
          src: 'http://alencontre.org/wp-content/uploads/2020/12/OpSentinelle.jpg',
          title: 'France. «Militaro-securité globale»: le jour d’après est déjà là',
          text: 'par Claude Serfati',
          url: 'http://alencontre.org/europe/france/militaro-securite-globale-le-jour-dapres-est-deja-la.html '
        },
        {
          src: 'https://www.cncdh.fr/sites/all/themes/cncdh_theme/images/avis.png',
          title: 'Proposition de loi sur la sécurité globale',
          text: 'La CNCDH s’alarme du contournement des processus démocratiques',
          url: 'https://www.cncdh.fr/fr/publications/proposition-de-loi-sur-la-securite-globale-la-cncdh-salarme-du-contournement-des'
        },
        {
          src: 'https://images.theconversation.com/files/376171/original/file-20201221-15-osp6nt.jpg',
          title: 'Loi sécurité globale : vers une privatisation de la police ?',
          text: 'par Fabien Bottini',
          url: 'https://theconversation.com/loi-securite-globale-vers-une-privatisation-de-la-police-152392'
        },
        {
          src: 'https://www.coe.int/documents/365513/10877703/letter-france-video-police-2020-870x489.jpg/8fb6da7d-41f0-083b-f12e-9d318408bcbc?t=1608218549000',
          title: 'Le Sénat français devrait modifier la proposition de loi relative à la sécurité globale pour en assurer la compatibilité avec les droits de l’homme',
          text: "par le Conseil de l'Europe",
          url: 'https://www.coe.int/fr/web/commissioner/-/the-french-senate-must-amend-the-general-security-bill-to-make-it-more-compatible-with-human-rights'
        },
        {
          src: 'http://www.syndicat-magistrature.org/local/cache-vignettes/L300xH99/siteon0-da8dd.png?1528697961',
          title: 'Proposition de loi « sécurité globale » : Vers un Etat de police ?',
          text: "par le Syndicat de la Magistrature",
          url: 'http://www.syndicat-magistrature.org/Proposition-de-loi-securite-globale-Vers-un-Etat-de-police.html'
        },
        {
          src: 'https://backend.streetpress.com/sites/default/files/field/image/panorama_de_lhemicyle_de_lassemblee_nationale_1.jpg',
          title: 'Loi Sécurité globale : Le petit cadeau du député retraité de la police aux retraités de la police',
          text: "Par Christophe-Cécil Garnier",
          url: 'https://www.streetpress.com/sujet/1607008993-loi-securite-globale-petit-cadeau-depute-retraite-police-retraites-fauvergue-article-conflit-interets'
        },
        {
          src: 'https://files.parisson.com/LSG/img/fauvergue.jpeg',
          title: 'Le député sécurise son emploi',
          text: "Par Le Canard Enchaîné",
          url: 'https://files.parisson.com/LSG/img/fauvergue.jpeg'
        },
        {
          src: 'https://www.laquadrature.net/wp-content/uploads/sites/8/2020/06/harme-scaled.jpeg',
          title: 'RACISME POLICIER : LES GÉANTS DU NET FONT MINE D’ARRÊTER LA RECONNAISSANCE FACIALE',
          text: "Par La Quadrature du Net",
          url: 'https://www.laquadrature.net/2020/06/22/racisme-policier-les-geants-du-net-font-mine-darreter-la-reconnaissance-faciale/'
        },
      ],
    }
  }
})
